#pragma once

#include "CEntity.h"

class CPlayer : public CEntity {
public:
	CPlayer();

	bool OnLoad(char* File, int Width, int Height, int MaxFrames, SDL_PixelFormat* form);

	void OnLoop(CEntityList&);

	void OnRender(SDL_Surface* Surf_Display);

	void OnCleanup();

	void OnAnimate();

	bool OnCollision(CEntity* Entity);
};
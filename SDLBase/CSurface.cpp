#include "CSurface.h"
#include <SDL_image.h>

CSurface::CSurface()
{
}

SDL_Surface * CSurface::OnLoad(char * File)
{
	return IMG_Load(File);
}

SDL_Surface * CSurface::OnLoad(char * File, SDL_PixelFormat* pixFormat)
{
	SDL_Surface* Surf_OptSurface = NULL;

	SDL_Surface* Surf_LoadedSurface = IMG_Load(File);

	if (Surf_LoadedSurface == NULL)
	{
		//Maybe log error
		return nullptr;
	}
	else {
		Surf_OptSurface = SDL_ConvertSurface(Surf_LoadedSurface, pixFormat, NULL);

		if (Surf_OptSurface == NULL)
		{
			//log error
			return Surf_LoadedSurface;	//If the Converting couldn't be done just parse out the unoptimized surface
		}
		SDL_FreeSurface(Surf_LoadedSurface);
	}

	return Surf_OptSurface;
}

bool CSurface::OnDraw(SDL_Surface * Surf_Dest, SDL_Surface * Surf_Src, int X, int Y)
{
	if (Surf_Dest == NULL || Surf_Src == NULL) {
		return false;
	}

	SDL_Rect DestR;

	DestR.x = X;
	DestR.y = Y;

	SDL_BlitSurface(Surf_Src, NULL, Surf_Dest, &DestR);

	return true;
}

bool CSurface::OnDraw(SDL_Surface * Surf_Dest, SDL_Surface * Surf_Src, int X, int Y, int X2, int Y2, int W, int H)
{
	if (Surf_Dest == NULL || Surf_Src == NULL) {
		return false;
	}

	SDL_Rect DestR;

	DestR.x = X;
	DestR.y = Y;

	SDL_Rect SrcR;

	SrcR.x = X2;
	SrcR.y = Y2;
	SrcR.w = W;
	SrcR.h = H;

	SDL_BlitSurface(Surf_Src, &SrcR, Surf_Dest, &DestR);

	return false;
}

bool CSurface::Transparent(SDL_Surface * Surf_Dest, int R, int G, int B)
{
	if (Surf_Dest == NULL) {
		return false;
	}

	SDL_SetColorKey(Surf_Dest, SDL_TRUE | SDL_RLEACCEL, SDL_MapRGB(Surf_Dest->format, R, G, B));

	return true;
}

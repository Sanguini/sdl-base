#pragma once

#include <SDL.h>

#include "Define.h"

enum {
	TARGET_MODE_NORMAL = 0,
	TARGET_MODE_CENTER
};

class CCamera {
private:
	float X;
	float Y;

	float* TargetX;
	float* TargetY;

public:
	int TargetMode;

public:
	CCamera();

public:
	void OnMove(float MoveX, float MoveY);

public:
	float GetX();
	float GetY();

public:
	void SetPos(float X, float Y);

	void SetTarget(float* X, float* Y);
};

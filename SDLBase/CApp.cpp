#include "CApp.h"
#include "CSurface.h"

//TODO: Add Errorlogger

CApp::CApp()
{
	Wnd_Screen = NULL;

	Surf_Display = NULL;

	bRunning = true;
}

CApp::~CApp()
{
	for(auto&& entity : EntityList)
		entity.OnCleanup();

	EntityList.clear();

	CArea::AreaControl.OnCleanup();

	SDL_FreeSurface(Surf_Display);
	SDL_Quit();
}

int CApp::OnExecute()
{
	if (OnInit() == false)
		return -1;

	SDL_Event Event;

	while (bRunning) {
		while (SDL_PollEvent(&Event)) {
			OnEvent(&Event);
		}

		OnLoop();
		OnRender();
	}

	//OnCleanup();

	return 0;
}

bool CApp::OnInit()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return false;

	Wnd_Screen = SDL_CreateWindow(WND_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WND_WIDTH, WND_HEIGHT, /*SDL_WINDOW_FULLSCREEN |*/ SDL_WINDOW_OPENGL);
	if (Wnd_Screen == NULL)
		return false;

	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
		return false;

	Surf_Display = SDL_GetWindowSurface(Wnd_Screen);
	if (Surf_Display == NULL)
		return false;

	if (Player.OnLoad("./yoshi.png", 64, 64, 8, Surf_Display->format) == false) {
		return false;
	}

	if (Player2.OnLoad("yoshi.png", 64, 64, 8, Surf_Display->format) == false) {
		return false;
	}

	Player2.X = 100;

	if (CArea::AreaControl.OnLoad("./maps/1.area", Surf_Display->format) == false) {
		return false;
	}

	EntityList.emplace_back(std::move(Player));
	EntityList.emplace_back(std::move(Player2));

	CCamera::CameraControl.TargetMode = TARGET_MODE_CENTER;
	CCamera::CameraControl.SetTarget(&EntityList[0].X, &EntityList[0].Y);

	return true;
}

void CApp::OnEvent(SDL_Event * Event)
{
	CEvent::OnEvent(Event);
}

void CApp::OnExit()
{
	bRunning = false;
}

void CApp::OnKeyDown(SDL_Keycode sym)
{
	switch (sym) {
	case SDLK_LEFT: {
		EntityList[0].MoveLeft = true;
		break;
	}

	case SDLK_RIGHT: {
		EntityList[0].MoveRight = true;
		break;
	}
	case SDLK_UP: {
		EntityList[0].Jump();
		break;
	}

	default: {
	}
	}
}

void CApp::OnKeyUp(SDL_Keycode sym)
{
	switch (sym) {
	case SDLK_LEFT: {
		EntityList[0].MoveLeft = false;
		break;
	}

	case SDLK_RIGHT: {
		EntityList[0].MoveRight = false;
		break;
	}

	default: {
	}
	}
}

void CApp::OnLoop()
{
	CFPS::FPSControl.OnLoop();

	for (auto&& entity : EntityList)
		entity.OnLoop(EntityList);


	//Collision Events
	for (auto&& entityCol : CEntityCol::EntityColList) {
		CEntity& EntityA = entityCol.EntityA;
		CEntity& EntityB = entityCol.EntityB;

		if (EntityA.OnCollision(EntityB)) {
			EntityB.OnCollision(EntityA);
		}
	}

	CEntityCol::EntityColList.clear();
}

void CApp::OnRender()
{
	SDL_FillRect(Surf_Display, NULL, 0xFFFFFF);
	CArea::AreaControl.OnRender(Surf_Display, -CCamera::CameraControl.GetX(), -CCamera::CameraControl.GetY());

	for (auto&& entity : EntityList)
		entity.OnRender(Surf_Display);

	SDL_UpdateWindowSurface(Wnd_Screen);
}

// void CApp::OnCleanup()
// {
// 	for (unsigned int i = 0;i < EntityList.size();i++) {
// 		if (!EntityList[i]) continue;
//
// 		EntityList[i]->OnCleanup();
// 	}
//
// 	EntityList.clear();
//
// 	CArea::AreaControl.OnCleanup();
//
// 	SDL_FreeSurface(Surf_Display);
// 	SDL_Quit();
// }

int main(int argc, char* argv[]) {
	CApp theApp;

	return theApp.OnExecute();
}


#pragma once

//GENERAL
#define WND_NAME "Geiled Game"
#define WND_WIDTH 640
#define WND_HEIGHT 480


//MAPPING
#define MAP_WIDTH    40
#define MAP_HEIGHT    40

#define MAX_ENTS 2048

#define TILE_SIZE    16

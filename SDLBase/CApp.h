#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <iostream>

#include "CPlayer.h"

#include "CEvent.h"

#include "CAnimation.h"
#include "CEntity.h"

#include "Define.h"

#include "CArea.h"
#include "CCamera.h"
#include "CFPS.h"

class CApp : public CEvent {	//Base class
private:
	bool    bRunning;	//basicly if the program should run

	//Window
	SDL_Window* Wnd_Screen;

	//Surface of the Window
	SDL_Surface* Surf_Display;

	std::vector<CEntity> EntityList;
	CCamera CameraControl;

	CPlayer Player;
	CPlayer Player2;

public:
	//Constructor
	CApp();
	~CApp();

	//First Call
	int OnExecute();

public:
	bool OnInit();

	void OnEvent(SDL_Event* Event);

	//Events
	void OnExit();
	void OnKeyDown(SDL_Keycode sym);
	void OnKeyUp(SDL_Keycode sym);

	//Game Logic stuff
	void OnLoop();

	void OnRender();

	//void OnCleanup();
};